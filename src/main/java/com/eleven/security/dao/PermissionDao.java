package com.eleven.security.dao;

import com.eleven.security.entity.Permission;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Description: PermissionDao
 * @Author: Eleven
 * @Date: 2021/9/1 23:29
 */
@SuppressWarnings("all")
@Mapper
public interface PermissionDao {
    List<Permission> findPermissionsByUserId(String userId);
}
