package com.eleven.security.dao;

import com.eleven.security.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description: UserDao
 * @Author: Eleven
 * @Date: 2021/8/29 15:40
 */
@SuppressWarnings("all")
@Mapper
public interface UserDao {
    User getUserByUserName(String userName);
}
