package com.eleven.security.service;

import com.eleven.security.entity.Permission;
import com.eleven.security.entity.User;

import java.util.List;

/**
 * @Description: UserService
 * @Author: Eleven
 * @Date: 2021/8/29 15:36
 */
@SuppressWarnings("all")
public interface UserService {
    User getUserByUserName(String userName);
    List<Permission> findPermissionsByUserId(String userId);
}
