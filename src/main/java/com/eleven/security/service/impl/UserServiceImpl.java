package com.eleven.security.service.impl;


import com.eleven.security.dao.PermissionDao;
import com.eleven.security.dao.UserDao;
import com.eleven.security.entity.Permission;
import com.eleven.security.entity.User;
import com.eleven.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description: UserServiceImpl
 * @Author: Eleven
 * @Date: 2021/8/29 15:39
 */
@SuppressWarnings("all")
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;
    @Autowired
    private PermissionDao permissionDao;

    @Override
    public User getUserByUserName(String userName) {
        return userDao.getUserByUserName(userName);
    }

    @Override
    public List<Permission> findPermissionsByUserId(String userId) {
        return permissionDao.findPermissionsByUserId(userId);
    }


}
