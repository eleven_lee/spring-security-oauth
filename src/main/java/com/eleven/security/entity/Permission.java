package com.eleven.security.entity;

import lombok.Data;

/**
 * @Description: Permission
 * @Author: Eleven
 * @Date: 2021/9/1 22:30
 */
@SuppressWarnings("all")
@Data
public class Permission {
    private String id;
    private String code;
    private String description;
    private String url;
}
