package com.eleven.security.entity;

import lombok.Data;

import java.util.Date;

/**
 * @Description: Role
 * @Author: Eleven
 * @Date: 2021/9/1 23:22
 */
@SuppressWarnings("all")
@Data
public class Role {
    private String id;
    private String roleName;
    private String description;
    private Date createTime;
    private Date updateTime;
    private String status;
}
