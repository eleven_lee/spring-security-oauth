package com.eleven.security.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description: User
 * @Author: Eleven
 * @Date: 2021/8/29 15:08
 */
@SuppressWarnings("all")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private String id;
    private String username;
    private String password;
    private String fullname;
    private String mobile;
}
