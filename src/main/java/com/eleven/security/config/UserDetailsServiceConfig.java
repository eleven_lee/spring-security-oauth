package com.eleven.security.config;

import com.eleven.security.entity.Permission;
import com.eleven.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description: UserDetailsService
 * @Author: Eleven
 * @Date: 2021/9/12 11:27
 */
@SuppressWarnings("all")
@Service
public class UserDetailsServiceConfig implements UserDetailsService {
    @Autowired
    private UserService userService;

    /**
     * @Description: Load User Infomation
     * @Author: Eleven
     * @Date: 2021/9/15 22:51
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // 根据用户名查询用户信息
        com.eleven.security.entity.User user = userService.getUserByUserName(username);
        // 根据用户ID查询用户权限
        List<Permission> permissionList = userService.findPermissionsByUserId(user.getId());
        List<String> permissionCodeList = permissionList.stream().map(Permission::getCode).collect(Collectors.toList());
        String[] perarray = new String[permissionCodeList.size()];
        // 设置用户信息
        UserDetails userDetails = User.withUsername(user.getFullname()).password(user.getPassword()).authorities(permissionCodeList.toArray(perarray)).build();
        return userDetails;
    }
}
