package com.eleven.security.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @Description: Spring Security Configuration
 * @Author: Eleven
 * @Date: 2021/9/10 8:50
 */
@SuppressWarnings("all")
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    /**
     * @Description: 配置密码编码器
     * @Author: Eleven
     * @Date: 2021/9/15 22:06
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * @Description: 配置安全拦截机制
     * @Author: Eleven
     * @Date: 2021/9/15 22:06
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
            .antMatchers("/r/r1").hasAnyAuthority("p1")
            .antMatchers("/r/r2").access("hasAuthority('p1') and hasAuthority('p2')")
            .antMatchers("/**").authenticated()
            .anyRequest().permitAll()
            .and()
            .formLogin()
            .successForwardUrl("/login‐success")
        ;
    }
}
