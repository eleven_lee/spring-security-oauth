package com.eleven.security.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Description: WebMvcConfigurer
 * @Author: Eleven
 * @Date: 2021/9/10 8:49
 */
@SuppressWarnings("all")
@Configuration
public class WebConfig implements WebMvcConfigurer {
    /**
     * @Description:
     *      Default url go to /login.
     *      The url was provided by spring-security.
     * @Author: Eleven
     * @Date: 2021/9/15 22:13
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("redirect:/login");
    }
}
