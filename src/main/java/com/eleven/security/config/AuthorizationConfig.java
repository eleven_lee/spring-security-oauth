package com.eleven.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;

/**
 * @Description: Oauth Authorization Configuration
 * @Author: Eleven
 * @Date: 2021/9/14 23:13
 */
@SuppressWarnings("all")
@Configuration
@EnableAuthorizationServer
public class AuthorizationConfig extends AuthorizationServerConfigurerAdapter {
    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * @Description:
     *      Client Configuration
     *      Get Authorization Code Url:
     *          http://localhost:9001/spring-security/oauth/authorize?client_id=mayikt&response_type=code
     *      Get Access Token Url:
     *          http://localhost:9001/spring-security/oauth/token?code=IkvdhN&grant_type=authorization_code&redirect_uri=http://www.baidu.com&scope=all
     *      Check The Token Validity Url:
     *          http://localhost:9001/spring-security/oauth/check_token?token=e3748fab-e5a8-48ea-af3f-c937232b025a
     * @Author: Eleven
     * @Date: 2021/9/15 23:02
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                // client id
                .withClient("mayikt")
                // client secret-key
                .secret(passwordEncoder.encode("123456"))
                // authority type
                .authorizedGrantTypes("authorization_code")
                // scope
                .scopes("all")
                // resource id
                .resourceIds("mayikt_resource")
                // call back url
                .redirectUris("http://www.baidu.com")
        ;
    }

    /**
     * @Description: Access Configuration
     * @Author: Eleven
     * @Date: 2021/9/17 8:05
     */
    @Override
    public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
        // Allow Form Authentication
        oauthServer.allowFormAuthenticationForClients();
        // Allow Check Token Url Access
        oauthServer.checkTokenAccess("permitAll()");
    }
}
