package com.eleven.security.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: LoginController
 * @Author: Eleven
 * @Date: 2021/9/10 8:54
 */
@SuppressWarnings("all")
@RestController
public class LoginController {
    /**
     * @Description: Login Success Url
     * @Author: Eleven
     * @Date: 2021/9/15 23:19
     */
    @RequestMapping(value = "/login‐success")
    public String loginSuccess(){
        String userName = getUsername();
        return userName + "登录成功.";
    }

    /**
     * @Description: 获取当前登录用户名
     * @Author: Eleven
     * @Date: 2021/9/15 23:17
     */
    private String getUsername(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(!authentication.isAuthenticated()){
            return null;
        }
        Object principal = authentication.getPrincipal();
        String username = null;
        if (principal instanceof org.springframework.security.core.userdetails.UserDetails) {
            username = ((org.springframework.security.core.userdetails.UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        return username;
    }

    /**
     * @Description: Test Resource 1
     * @Author: Eleven
     * @Date: 2021/9/15 23:18
     */
    @GetMapping(value = "/r/r1")
    public String r1(){
        String username = getUsername();
        return username + "访问资源1";
    }

    /**
     * @Description: Test Resource 2
     * @Author: Eleven
     * @Date: 2021/9/15 23:18
     */
    @GetMapping(value = "/r/r2")
    public String r2(){
        String username = getUsername();
        return username + "访问资源2";
    }
}
